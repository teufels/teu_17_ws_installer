#!/bin/bash

#######################################
## Installation for TYPO3
#######################################

version="v.8.0.0";

install() {
    startCounter=$(date +%s)
    clear

    infoMsg ":: HIVE | installer | $version\n" \
    "::"
    nbsp

    if [ ! -f "install.ini" ]; then
       errorMsg ":: Error [g3978p78jzk312a0el4v1nd4ut6tb3z8] in configuration\n" \
        ":: File <install.ini> does not exist\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    #include config to create a new project
    . install.ini

    if [ ! -f "install.local.ini" ]; then
       errorMsg ":: Error [g3978p78jzk312a0el4v1nd4ut6tb3z8] in configuration\n" \
        ":: File <install.local.ini> does not exist\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    #include config to create a new project
    . install.local.ini

    boilerplatename=$hive_boilerplate
    phpContainer=$phpcontainer
    repoThm=$repothm

    infoMsg "::\n" \
    ":: Version: $version\n" \
    "::\n" \
    ":: Boilerplate Folder: $(tput setaf 6)$boilerplatename$(tput sgr 0)\n" \
    ":: PHP Container: $(tput setaf 6)$phpcontainer$(tput sgr 0)\n" \
    ":: Thm Repository: $(tput setaf 6)$repothm$(tput sgr 0)\n" \
    "::\n"


    while true; do
        infoMsg "::\n" \
        ":: Are these values correct? $(tput setaf 6)y/n$(tput sgr 0)"
        read yn
        case $yn in
            [Yy]* ) break;;
            [Nn]* ) exit;;
            * ) echo -e "\n:: Please answer yes or no.\n::\n";;
        esac
    done
    nbsp


    #check placeholder variables
    if [ "$repoThm" == "XXX_YY_xxx_thm" ]; then
        errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \$repothm must not be '$repoThm'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    if [ "$repoThm" == "hive_thm_custom" ]; then
       errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \$repothm must not be '$repoThm'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    if [ "$boilerplatename" == "hive_boilerplate_YY-mm-dd_HH-MM-SS" ]; then
       errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \boilerplatename must not be '$boilerplatename'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    #check if docker-compose.yml exists
    if [ ! -f "../${boilerplatename}/docker-compose.yml" ]; then
       errorMsg ":: Error [g3978p78jzk312a0el4v1nd4ut6tb3z8] in configuration\n" \
        ":: File <../${boilerplatename}/docker-compose.yml> does not exist\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    #check if docker-sync.yml doesn't exists
    if [ -f "../${boilerplatename}/docker-sync.yml" ]; then
    errorMsg ":: Error [fsf9qxy3i3pi4k4usi18umx21r9q837v] in configuration\n" \
        ":: File <../${boilerplatename}/docker-sync.yml> does exist\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    logMsg "install TYPO3"


    cd src

    ###### create database .ini files ######
    logMsg "create database .ini files"
    cp -v "expample.ini" "development.ini"
    cp -v "expample.ini" "staging.ini"
    cp -v "expample.ini" "production1.ini"
    cp -v "expample.ini" "production.ini"
    cp -v "expample.ini" "hotfix.ini"
    nbsp

    ###### start docker ######
    cd ../../${boilerplatename}

    #stop all running docker containers
    logMsg "stopping all running docker containers"
    docker stop $(docker ps -a -q)
    nbsp
    sleep 2


    #start docker
    logMsg "start docker pull $phpContainer"
    docker pull ${phpContainer}
    nbsp
    sleep 12


    #start docker
    logMsg "start docker-compose up -d"
    docker-compose up -d
    nbsp
    sleep 12


    ###### copy composer.json with forked thm ######
    cd ../hive_installer/src/app
    logMsg "set thm variable to composer.json"
    sed -ie "s/XXX_YY_xxx_thm/$repoThm/" composer.json
    infoMsg "::\n" \
    ":: diff composer.jsone composer.json\n" \
    "::"
    nbsp
    infoMsg "-+"
    diff -by -W245 --suppress-common-lines ./composer.jsone ./composer.json
    infoMsg "-+"
    nbsp

    rm -rf composer.jsone

    logMsg "copy composer.json to ${boilerplatename}"
    cp -n "composer.json" "../../../${boilerplatename}/app/composer.json"
    nbsp
    sleep 1


    ###### composer install ######
    cd ../../../${boilerplatename}/app
	composer install
	nbsp
    sleep 1

    logMsg "create FIRST_INSTALL file"
	cd web
	touch FIRST_INSTALL
	sleep 1


#    ###### copy dummy database and install_extensions.sh to hive_boilerplate directories ######
#    logMsg "copy dummy database and install_extensions.sh to ${boilerplatename}"
#    cd ../../..
#
#    if [ ! -d "$boilerplatename/backup" ]; then
#       mkdir ${boilerplatename}/backup
#    fi
#
#    cp -v "hive_installer/src/TYPO3dummy.sql" "${boilerplatename}/backup/TYPO3dummy.sql"
#    cp -v "hive_installer/src/install_extensions.sh" "${boilerplatename}/app/install_extensions.sh"
#    sleep 1

    cd ../../..

    ###### copy src files to hive_boilerplate directories ######
    logMsg "copy files to ${boilerplatename}"

    cp -v "hive_installer/src/app/web/.htaccess" "${boilerplatename}/app/web/.htaccess"
    cp -v "hive_installer/src/app/web/typo3conf/local.ini" "${boilerplatename}/app/web/typo3conf/local.ini"
    cp -v "hive_installer/src/app/web/typo3conf/AdditionalConfiguration.php" "${boilerplatename}/app/web/typo3conf/AdditionalConfiguration.php"
    cp -v "hive_installer/src/app/web/typo3conf/PackageStates.php" "${boilerplatename}/app/web/typo3conf/PackageStates.php"
    cp -v "hive_installer/src/app/gulpfile.js" "${boilerplatename}/app/gulpfile.js"
    nbsp

    ###### browser configuration ######
    open "http://development.localhost/typo3/sysext/install/Start/Install.php"
    logMsg "open http://development.localhost/typo3/sysext/install/Start/Install.php"
    nbsp
    nbsp
    infoMsg "::\n" \
    "::\n" \
    ":: Switch to the browser to complete the next installation steps\n" \
    ":: Then continue the installation" \
    "::\n"

	while true; do
        infoMsg "::\n" \
        ":: Continue the installation? $(tput setaf 6)y/n$(tput sgr 0)"
        read yn
        case $yn in
            [Yy]* ) break;;
            [Nn]* ) exit;;
            * ) echo -e "\n:: Please answer yes or no.\n::\n";;
        esac
    done
    nbsp

    docker ps

    nbsp

    while true; do
        infoMsg "::\n" \
        ":: Is docker still running? $(tput setaf 6)y/n$(tput sgr 0)"
        read yn
        case $yn in
            [Yy]* ) break;;
            [Nn]* ) exit;;
            * ) echo -e "\n:: Please answer yes or no.\n::\n";;
        esac
    done
    nbsp

#    ###### import dummy database ######
#    logMsg "start dummy database import"
#    cd ${boilerplatename}
#    make -f Makefile.teufels mysql-import-TYPO3dummy
#    sleep 1


#    ###### install extensions ######
#    logMsg "install default extensions"
#    make -f Makefile.teufels install-extensions
#	sleep 1


#	###### copy cooluriconf ######
#    logMsg "copy CoolUriConf.xml to typo3conf folder"
#    cp -v app/web/typo3conf/ext/cooluri/Resources/CoolUriConf.xml_default app/web/typo3conf/CoolUriConf.xml

    #cd ..


    ###### delete symlink ######
    logMsg "delete index.php symlink"
    rm -rf ${boilerplatename}/app/web/index.php
    sleep 1


    ###### copy index.php ######
    logMsg "copy index.php to ${boilerplatename}"
    cp -v "hive_installer/src/app/web/index.php" "${boilerplatename}/app/web/index.php"

    ###### clear cache and set permissions ######
    nbsp
    logMsg "clear cache and set permissions"
    rm -rf ${boilerplatename}/app/web/typo3temp/*
    chmod -R 777 ${boilerplatename}/app/web/typo3temp/
    chmod -R 777 ${boilerplatename}/app/web/uploads/
    chmod -R 777 ${boilerplatename}/app/web/fileadmin/
    chmod -R 777 ${boilerplatename}/app/web/typo3conf/LocalConfiguration.php
    chmod -R 777 ${boilerplatename}/app/web/typo3conf/PackageStates.php
    chmod -R 777 ${boilerplatename}/app/web/typo3conf/AdditionalConfiguration.php
    cd hive_installer

    logMsg "script finished"
    endCounter=$(date +%s)
    totalTime=$(echo "$endCounter - $startCounter" | bc)
    echo " total time:" $(convertsecs $totalTime)
}

convertsecs() {
    h=`expr $1 / 3600`
    m=`expr $1  % 3600 / 60`
    s=`expr $1 % 60`
    printf "%02d:%02d:%02d\n" $h $m $s
}

errorMsg() {
    echo -e "" \
    $(tput setaf 1)::$(tput sgr 0) "\n"\
    $(tput setaf 1)$*$(tput sgr 0) "\n"\
    $(tput setaf 1)::$(tput sgr 0)
}

logMsg() {
    echo -e "" \
    "$*"
}

infoMsg() {
    echo -e "" \
    "$*"
}

nbsp() {
    echo ""
}

install